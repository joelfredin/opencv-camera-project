import numpy as np
import cv2 as cv
import time
import math
img2 = np.zeros((512,512,3), np.uint8)

print('Original Dimensions : ',img2.shape)

scale_percent = 60 # percent of original size
width = int(img2.shape[1] * scale_percent / 100)
height = int(img2.shape[0] * scale_percent / 100)
dim = (width, height)

# resize image
resized = cv.resize(img2, dim, interpolation = cv.INTER_AREA)

print('Resized Dimensions : ',resized.shape)


#cv.ellipse(frame,(256,256),(100,50),0,0,180,255,-1)



#cv.imshow("Resized image", resized)
#cv.waitKey()
#cv.destroyAllWindows()
cap = cv.VideoCapture(0)
t0 = time.time()
# Define the codec and create VideoWriter object
fourcc = cv.VideoWriter_fourcc(*'XVID')
out = cv.VideoWriter('output2.avi', fourcc, 20.0, (640,  480))
x = 50
y = 50
while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break
    #cv.ellipse(img,(256,256),(100,50),0,0,180,255,-1)
    font = cv.FONT_HERSHEY_SIMPLEX

    #frame[ 10,10 ] = img2
    # write the flipped frame
    out.write(frame)
    cv.imshow('frame', frame)
    #cv.imshow('frame',gray)
    #cv.imshow('frame',img2)
    #cv.waitKey(1250)
    #cv.destroyAllWindows()
    t1 = time.time()
    num_seconds = t1 - t0
    if num_seconds < 10:
        #break
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        frame[:,:,0] = gray
        frame[:,:,1] = gray
        frame[:,:,2] = gray
        frame = cv.circle(frame,(220,63), 63-2*math.floor(num_seconds), (0,0,255), -1)
        frame = cv.circle(frame,(220,63), 50-4*math.floor(num_seconds), (0,255,0), -1)
        frame = cv.circle(frame,(70,63), 63-2*math.floor(num_seconds), (0,0,255), -1)
        frame = cv.circle(frame,(70,63), 50-4*math.floor(num_seconds), (0,255,0), -1)
        frame = cv.ellipse(frame,(206,113),(100,40),0,0,140+5*math.floor(num_seconds),(48, 25, 52),5)
        out.write(frame)
        cv.imshow('frame', frame)
    if 10 < num_seconds < 20:
        frame = cv.ellipse(frame,(256,256),(100,50),0-num_seconds,0+2*num_seconds,180+10*num_seconds,255-20*num_seconds,-1)
        frame = cv.circle(frame,(447,63), 63, (0,0,255), -1)
        frame = cv.circle(frame,(50,63), 63-2*math.floor(num_seconds-10), (0,0,255), -1)
        frame = cv.circle(frame,(50,63), 50-2*math.floor(num_seconds-10), (0,255,0), -1)
        frame = cv.circle(frame,(0,63), 63-2*math.floor(num_seconds-10), (0,0,255), -1)
        frame = cv.circle(frame,(0,63), 50-2*math.floor(num_seconds-10), (0,255,0), -1)
        if math.floor(num_seconds)%2 == 0:
            frame = cv.flip(frame, 1)
        if math.floor(num_seconds)%3 == 1:
            frame = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
            frame = cv.flip(frame, 0)
        out.write(frame)
        cv.imshow('frame', frame)
    if 20 < num_seconds < 30:
        #frame = cv.flip(frame, 0)
        #image1 = cv.ellipse(frame,(256,256),(100,50),0,0,180,255,-1)
        #image2 = cv.circle(frame,(447,63), 300, (0,0,255), -1)
        #image1[100,100] = [255,255,255]
        #ball = image1[255:315, 270:320]
        #frame[225:315, 270:320] = ball
        #frame = cv.addWeighted(image1, 0.7, image2, 0.3, 0)
        font = cv.FONT_HERSHEY_SIMPLEX

        # org
        org = (50, 50)

        # fontScale
        fontScale = 1

        # Blue color in BGR
        color = (255, 0, 0)
        color2 = (0,255,0)

        # Line thickness of 2 px
        thickness = 2
        #image1 = cv.putText(image1, 'OpenCV', org, font,
        #           fontScale, color, thickness, cv.LINE_AA)
        #image2 = cv.putText(image1, 'HALLE', org, font,
        #           fontScale, color2, thickness, cv.LINE_AA)
        #image2 = cv.imread('./the_real_retro.png')
        #image3 = cv.imread('./my_quiz_game.png')
        #Nimg1 = image1.resize((20,20))
        #Nimg2 = image2.resize((20,20))
        #frame = cv.addWeighted(Nimg1,0.7, Nimg2,0.3,0)
        #ROI = image[y:y+h, x:x+w]
        image = cv.imread('mapna.jpg')
        image2 = cv.imread('retro_image.jpg')
        image = image[0:200, 0:200]
        image2 = image2[100+10*math.floor(num_seconds):300+10*math.floor(num_seconds), 100+10*math.floor(num_seconds):300+10*math.floor(num_seconds)]
        image3 = cv.addWeighted(image,0.1, image2,0.9,0)
        #Nimg1 = image.resize((250,250))
        #Nimg2 = image2.resize((250,250))
        #Nimg1 = image.resize((20,20))
        ball = image3[0:200, 0:200]
        frame[0:200, 0:200] = ball
        out.write(frame)
        cv.imshow('frame', frame)
    if 30 < num_seconds < 40:
        #frame = cv.flip(frame, 0)
        frame = cv.ellipse(frame,(256,256),(100,50),0,0,180,255,-1)
        frame = cv.circle(frame,(447,63), 63, (0,0,255), -1)
        out.write(frame)
        cv.imshow('frame', frame)

    if 40 < num_seconds < 50:
        frame = cv.circle(frame,(170,63), 63-2*math.floor(num_seconds-40), (0,0,255), -1)
        frame = cv.circle(frame,(170,63), 50-4*math.floor(num_seconds-40), (0,255,0), -1)
        frame = cv.circle(frame,(120,63), 63-2*math.floor(num_seconds-40), (0,0,255), -1)
        frame = cv.circle(frame,(120,63), 50-4*math.floor(num_seconds-40), (0,255,0), -1)
        frame = cv.ellipse(frame,(256,113),(100,40),0,0,140+5*math.floor(num_seconds),(48, 25, 52),5)
    if cv.waitKey(1) == ord('q'):
        break
# Release everything if job is finished



cap.release()
out.release()
cv.destroyAllWindows()
